#ifndef ENEMY_FACTORY_HPP_
#define ENEMY_FACTORY_HPP_

#include "monsters.hpp"

namespace Game
{

class AbstractEnemyFactory
{
public:
	virtual Soldier* CreateSoldier() = 0;
	virtual Monster* CreateMonster() = 0;
	virtual SuperMonster* CreateSuperMonster() = 0;
	virtual ~AbstractEnemyFactory() {}
};

class EasyLevelEnemyFactory : public AbstractEnemyFactory
{
public:
	virtual Soldier* CreateSoldier()
	{
		return new SillySoldier();
	}
	
	virtual Monster* CreateMonster()
	{
		return new SillyMonster();
	}
	
	virtual SuperMonster* CreateSuperMonster()
	{
		return new SillySuperMonster();
	}
};

class DieHardLevelEnemyFactory : public AbstractEnemyFactory
{
public:
	virtual Soldier* CreateSoldier()
	{
		return new BadSoldier();
	}
	
	virtual Monster* CreateMonster()
	{
		return new BadMonster();
	}
	
	virtual SuperMonster* CreateSuperMonster()
	{
		return new BadSuperMonster();
	}
};


}


#endif /*ENEMY_FACTORY_HPP_*/
