#ifndef EMPLOYEE_HPP_
#define EMPLOYEE_HPP_

#include <iostream>
#include <string>
#include <boost/shared_ptr.hpp>

class HRInfo;

class Employee
{

public:
	virtual void description() const = 0;
    virtual boost::shared_ptr<HRInfo> create_hrinfo() const = 0;
	virtual ~Employee();
};

class EmployeeBase : public Employee
{
protected:
    std::string name_;
public:
    EmployeeBase(const std::string& name);
    virtual boost::shared_ptr<HRInfo> create_hrinfo() const;
};

class Salary : public EmployeeBase
{
public:
	Salary(const std::string& name);

	virtual void description() const;
};

class Hourly : public EmployeeBase
{
public:
	Hourly(const std::string& name);

	virtual void description() const;
};

class Temp : public EmployeeBase
{
public:
	Temp(const std::string& name);

	virtual void description() const;
    virtual boost::shared_ptr<HRInfo> create_hrinfo() const;
};

#endif /* EMPLOYEE_HPP_ */
