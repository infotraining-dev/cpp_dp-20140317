#include "employee.hpp"
#include "hrinfo.hpp"

Employee::~Employee()
{
}

EmployeeBase::EmployeeBase(const std::string& name) : name_(name)
{
}

boost::shared_ptr<HRInfo> EmployeeBase::create_hrinfo() const
{
    return boost::shared_ptr<HRInfo>(new StdInfo(this));
}

Salary::Salary(const std::string& name) : EmployeeBase(name)
{
}

void Salary::description() const
{
	std::cout << "Salaried Employee: " << name_ << std::endl;
}

Hourly::Hourly(const std::string& name) : EmployeeBase(name)
{
}

void Hourly::description() const
{
	std::cout << "Hourly Employee: " << name_ << std::endl;
}

Temp::Temp(const std::string& name) : EmployeeBase(name)
{
}

void Temp::description() const
{
	std::cout << "Temporary Employee: " << name_ << std::endl;
}

boost::shared_ptr<HRInfo> Temp::create_hrinfo() const
{
    return boost::shared_ptr<HRInfo>(new TempInfo(this));
}
