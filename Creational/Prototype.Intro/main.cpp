#include <iostream>
#include <cassert>
#include <typeinfo>

using namespace std;

class Engine
{
public:
    virtual void start() = 0;
    virtual void stop() = 0;
    virtual Engine* clone() const = 0;
    virtual ~Engine() {}
};

template <typename EngineType>
class ClonableEngine : public Engine
{
public:
    Engine* clone() const
    {
        return new EngineType(*static_cast<const EngineType*>(this));
    }
};


class Diesel : public ClonableEngine<Diesel>
{
public:
    virtual void start()
    {
        cout << "Diesel starts\n";
    }

    virtual void stop()
    {
        cout << "Diesel stops\n";
    }
};

class TDI : public Diesel
{
public:
    virtual void start()
    {
        cout << "TDI starts\n";
    }

    virtual void stop()
    {
        cout << "TDI stops\n";
    }
};

class Hybrid : public ClonableEngine<Hybrid>
{
public:
    virtual void start()
    {
        cout << "Hybrid starts\n";
    }

    virtual void stop()
    {
        cout << "Hybrid stops\n";
    }
};

class Car
{
    Engine* engine_;
public:
    Car(Engine* engine) : engine_(engine)
    {}

    Car(const Car& source) : engine_(source.engine_->clone())
    {
    }

    ~Car() { delete engine_; }

    void drive(int km)
    {
        engine_->start();
        cout << "Driving " << km << " kms\n";
        engine_->stop();
    }
};

int main()
{
    Car c1(new TDI());

    c1.drive(100);

    cout << "\n";

    Car copy_of_car = c1;

    copy_of_car.drive(200);
}

