#ifndef PHOENIX_SINGLETON_HPP_
#define PHOENIX_SINGLETON_HPP_

#include <stdexcept>
#include <cstdlib>

namespace Phoenix
{

template <typename T>
class SingletonHolder
{
public:
	static T& instance()
	{
		if (!p_instance_)
		{
			if (destroyed_)
			{
				on_dead_reference();
			}
			else
			{
				create();
			}
		}
		return *p_instance_;
	}

private:
	union MaxAlign
	{
		char t_[sizeof(T)];
	    short int shortInt_;
	    int int_;
	    long int longInt_;
	    float float_;
	    double double_;
	    long double longDouble_;
	    struct Test;
	    int Test::* pMember_;
	    int (Test::*pMemberFn_)(int);
	};

	static void create()
	{
		static MaxAlign static_memory_;
		p_instance_ = new (&static_memory_)T;
		std::atexit(&destroy);
	}

	static void destroy()
	{
		p_instance_->~T();
		p_instance_ = 0;
		destroyed_ = true;
	}

	static void on_dead_reference()
	{
		std::cout << "Dead reference encountered..." << std::endl;
		std::cout << "Creating a phoenix object..." << std::endl;
		create();
		destroyed_ = false;
	}
private:
	SingletonHolder();
	SingletonHolder(const SingletonHolder&);
	SingletonHolder& operator=(const SingletonHolder&);
	static T* p_instance_;
	static bool destroyed_;
};

template <typename T>
T* SingletonHolder<T>::p_instance_ = 0;

template <typename T>
bool SingletonHolder<T>::destroyed_ = false;

}
#endif /* PHOENIX_SINGLETON_HPP_ */
