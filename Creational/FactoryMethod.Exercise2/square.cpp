#include "shape_factory.hpp"
#include "square.hpp"

namespace
{
    using namespace Drawing;

    bool is_registered
        = ShapeFactory::instance()
            .register_creator("Square", ShapeCreator<Square>());
}
