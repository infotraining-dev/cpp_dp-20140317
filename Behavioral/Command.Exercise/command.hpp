#ifndef COMMAND_HPP_
#define COMMAND_HPP_

#include <iostream>
#include <string>
#include <stack>

#include "document.hpp"

class Command
{
public:
	virtual void execute() = 0;
	virtual void undo() = 0;
	virtual Command* clone() = 0;

	virtual ~Command() {}
};

class PasteCmd : public Command
{
	Document* document_;
	size_t prev_length_;

public:
	PasteCmd(Document* document) : document_(document), prev_length_(0)
	{
	}

	void execute()
	{
		prev_length_ = document_->length();
		document_->push_command( clone() );

		document_->paste();
	}

	void undo()
	{
		size_t replacement_length = document_->length() - prev_length_;
		document_->replace(prev_length_, replacement_length, "");
	}

	PasteCmd* clone()
	{
		return new PasteCmd(*this);
	}
};

class CopyCmd // TODO
{
};

class ToUpperCmd : public Command
{
	Document* document_;
	Document::Memento memento_;

public:
	ToUpperCmd(Document* document) : document_(document)
	{
	}

	void execute()
	{
		memento_ = document_->create_memento();
		document_->push_command( clone() );

		document_->to_upper();
	}

	void undo()
	{
		document_->set_memento(memento_);
	}

	ToUpperCmd* clone()
	{
		return new ToUpperCmd(*this);
	}
};

class ToLowerCmd // TO DO
{
};

class PrintCmd : public Command
{
	Document* document_;
public:
	PrintCmd(Document* document) : document_(document)
	{
	}

	void execute()
	{
		document_->print();
	}

	void undo()
	{
	}

	PrintCmd* clone()
	{
		return new PrintCmd(*this);
	}
};

class AddTextCmd // TODO
{

};


class UndoCmd : public Command
{
	Document* document_;
public:
	UndoCmd(Document* doc) : document_(doc) {}

	void execute()
	{
		try
		{
			Command* prevCmd = document_->pop_command();
			prevCmd->undo();
			delete prevCmd;
		}
		catch(const std::out_of_range& e)
		{
			std::cout << e.what() << std::endl;
		}
	}

	void undo()
	{
		// brak implementacji
	}

	UndoCmd* clone()
	{
		return new UndoCmd(*this);
	}
};
#endif /*COMMAND_HPP_*/
