#include "shape.hpp"
#include "shape_factory.hpp"
#include <vector>
#include <algorithm>
#include <functional>
#include <fstream>

using namespace std;
using namespace Drawing;

// Client depends on Shapes
class GeoApp
{
	vector<Shape*> shapes_;
public:
	void draw()
	{
		cout << "\nDrawing:\n";
		for_each(shapes_.begin(), shapes_.end(), mem_fun(&Shape::draw));
	}

	void read_file(const std::string& filename)
	{
		ifstream fin(filename.c_str());

		string type_identifier;

		cout << "Loading a file...\n";
		while(!fin.eof())
		{
			if (fin >> type_identifier)
			{
				cout << type_identifier << "\n";
				Shape* shp_ptr = ShapeFactory::instance().create(type_identifier);
				shp_ptr->read(fin);

				shapes_.push_back(shp_ptr);
			}
		}

		cout << "Loading finished." << endl;
	}

	void save_file(const std::string& filename)
	{
		// TODO: // Zastosuj wzorzez Visitor do zapisania rysunku w formacie csv
	}

	~GeoApp()
	{
		for(vector<Shape*>::iterator it = shapes_.begin(); it != shapes_.end(); ++it)
			delete *it;
	}
};



int main()
{
	GeoApp app;
	app.read_file("drawing.txt");
	app.draw();
}

