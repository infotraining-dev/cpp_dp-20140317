#ifndef COFFEEHELL_HPP_
#define COFFEEHELL_HPP_

#include <iostream>
#include <string>
#include <boost/noncopyable.hpp>
#include <boost/utility/base_from_member.hpp>

class Coffee
{
protected:
    float price_;
    std::string description_;
public:
    Coffee(float price, const std::string& description) : price_(price), description_(description)
    {
    }

    virtual float get_total_price() const
    {
        return price_;
    }

    virtual std::string get_description() const
    {
        return description_;
    }

    virtual void prepare() = 0;


    virtual ~Coffee() {}
};

class Espresso : public Coffee
{
public:
    Espresso(float price = 4.0, const std::string& description = "Espresso")
        : Coffee(price, description)
    {
    }

    void prepare()
    {
        std::cout << "Making a perfect espresso: 7 g, 15 bar and 24 sec.\n";
    }
};

class Cappuccino : public Coffee
{
public:
    Cappuccino(float price = 6.0, const std::string& description = "Cappuccino")
        : Coffee(price, description)
    {
    }

    void prepare()
    {
        std::cout << "Making a perfect cappuccino.\n";
    }
};

class Latte : public Coffee
{
public:
    Latte(float price = 8.0, const std::string& description = "Latte")
        : Coffee(price, description)
    {
    }

    void prepare()
    {
        std::cout << "Making a perfect latte.\n";
    }
};

class CoffeeDecorator : public Coffee, boost::noncopyable
{
protected:
    Coffee* coffee_;
public:
    CoffeeDecorator(Coffee* coffee, float price, const std::string& desc)
        : Coffee(price, desc), coffee_(coffee)
    {
    }

    ~CoffeeDecorator()
    {
        delete coffee_;
    }

    virtual float get_total_price() const
    {
        return price_ + coffee_->get_total_price();
    }

    virtual std::string get_description() const
    {
        return coffee_->get_description() + " + " + description_;
    }

    virtual void prepare()
    {
        coffee_->prepare();
    }
};

class Whipped : public CoffeeDecorator
{
public:
    Whipped(Coffee* coffee) : CoffeeDecorator(coffee, 2.5, "Whipped Cream")
    {
    }

    void prepare()
    {
        CoffeeDecorator::prepare();
        std::cout << "Adding a whipped cream..." << std::endl;
    }
};

class Whisky : public CoffeeDecorator
{
public:
    Whisky(Coffee* coffee) : CoffeeDecorator(coffee, 6.0, "Whisky")
    {
    }

    void prepare()
    {
        CoffeeDecorator::prepare();
        std::cout << "Pouring 50cl of whisky..." << std::endl;
    }
};

class ExtraEspresso :
        boost::base_from_member<Espresso>,
        public CoffeeDecorator
{
public:
    ExtraEspresso(Coffee* coffee)
        : CoffeeDecorator(coffee, member.get_total_price(), "Extra espresso")
    {
    }

    void prepare()
    {
        CoffeeDecorator::prepare();
        member.prepare();
    }
};

#endif /*COFFEEHELL_HPP_*/
