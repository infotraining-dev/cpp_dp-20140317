#include "composite.hpp"

using namespace boost;

int main()
{
	// Create a tree structure
	shared_ptr<Component> root(new Composite("root"));

	root->add(shared_ptr<Component>(new Leaf("Leaf A")));
	root->add(shared_ptr<Component>(new Leaf("Leaf B")));

	shared_ptr<Component> comp(new Composite("Composite X"));
	comp->add(shared_ptr<Component>(new Leaf("Leaf XA")));
	comp->add(shared_ptr<Component>(new Leaf("Leaf XB")));

	root->add(comp);
	root->add(shared_ptr<Component>(new Leaf("Leaf C")));

	// Add and remove a leaf
	boost::shared_ptr<Leaf> leaf(new Leaf("Leaf D"));
	root->add(boost::dynamic_pointer_cast<Component>(leaf));
	root->remove(boost::dynamic_pointer_cast<Component>(leaf));

	// Recursively display tree
	root->display(1);

	std::cout << "\n\n---------------------\n\n";
}
