#ifndef DECORATOR_HPP_
#define DECORATOR_HPP_

#include <iostream>
#include <string>
#include <boost/noncopyable.hpp>

// "Component"
class Component
{
public:
	virtual void operation() = 0;
	virtual ~Component() {};
};


// "ConcreteComponent"
class ConcreteComponent : public Component
{
public:
	void operation()
	{
		std::cout << "ConcreteComponent.operation()";
	}
};


// "Decorator"
class Decorator : public Component, boost::noncopyable
{
protected:
	Component* component_;
public:
	Decorator(Component* component) : component_(component)
	{
	}

    ~Decorator()
    {
        delete component_;
    }
	
	void set_component(Component* component)
	{
        delete component_;
		component_ = component;
	}
	
	void operation()
	{
		component_->operation();
	}
};


// "ConcreteDecoratorA" 
class ConcreteDecoratorA : public Decorator
{
private:
	std::string added_state_;
public:
	ConcreteDecoratorA(Component* component) : Decorator(component)
	{
	}
	void operation()
	{
		Decorator::operation();
		added_state_ = "added state";
		std::cout << " and decorated with " << added_state_;
	}
};

// "ConcreteDecoratorB"
class ConcreteDecoratorB : public Decorator
{
private:
	std::string added_behaviour()
	{
		return std::string("added behaviour");
	}
public:
	ConcreteDecoratorB(Component* component) : Decorator(component)
	{
	}
	
	void operation()
	{
		Decorator::operation();
		std::cout << " and decorated with " << added_behaviour();
	}
};

#endif /*DECORATOR_HPP_*/
