#include "image.hpp"
#include "clone_factory.hpp"

namespace
{
    bool is_registered = Drawing::ShapeFactory::instance()
            .register_shape("Image", new Drawing::ImageProxy());
}
